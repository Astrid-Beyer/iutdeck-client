using System;
using System.Text;
using System.Net.Sockets;

public class OneShotConnector : IConnector {
    
    public int    port {get;set;}
    public string host {get;set;}

    public OneShotConnector(int port, string host) {
        this.port    = port;
        this.host    = host;
    }

    public IGameMessage send(IGameMessage message) {
        TcpClient tcpClient;
        NetworkStream ns = null;
        tcpClient = new TcpClient(this.host, this.port);

        try { // tentative de connexion au flux
            ns = tcpClient.GetStream();
        } catch {
            Console.WriteLine("Impossible de se connecter au réseau, sortie du flux.");
                Environment.Exit(1);
        }

        Console.WriteLine("Connexion au flux réussie.");

        byte[] receiveBuffer = new byte[1024];
        byte[] sendBuffer;
        int bytesRead = 0;

        sendBuffer      = Encoding.ASCII.GetBytes(message.message());            // Transforme le message en suite d'octets
        ns.Write(sendBuffer, 0, sendBuffer.Length);                              // On écrit la suite d'octets dans le flux
        bytesRead       = ns.Read(receiveBuffer, 0, receiveBuffer.Length);       // On récupère la réponse du flux sous forme d'octets
        string response = Encoding.ASCII.GetString(receiveBuffer, 0, bytesRead); // Décode les données

        ns.Flush();
        ns.Close();
        ns.Dispose();
        tcpClient.Close();
        return GameClient.readMessage(response);
    }
}