using System;
using System.Text;
using System.Net.Sockets;

public class ObjectChange : IGameMessage {
    public string object_id {get;set;}
    public string properties {get;set;}

    public ObjectChange(string object_id, string properties) { 
        this.object_id    = object_id;
        this.properties = properties;
    }
    public string message() {
        string retour = "{\"name\" : \"object_change\",\"properties\" : {\"object_id\" : \"" + this.object_id + "\",\"properties\" : \"" + this.properties + "\"}}";
        return retour;
    }
}