using System;
using System.Text;
using System.Net.Sockets;

public class JoinRoomOffline : IGameMessage {
    public int roomId {get;set;}
    public string pseudonym {get;set;}

    public JoinRoomOffline(int roomId, string pseudonym) { 
        this.roomId    = roomId;
        this.pseudonym = pseudonym;
    }
    public string message() {
        string retour = "{\"name\" : \"joinRoomOff\",\"properties\" : {\"roomId\" : \"" + this.roomId + "\",\"pseudo\" : \"" + this.pseudonym + "\"}}";
        return retour;
    }
}