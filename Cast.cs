using System;
using System.Text;
using System.Net.Sockets;

public class Cast : IGameMessage {
    public string objectId {get;set;}
    public string targetId {get;set;}

    public Cast(string objectId, string targetId) { 
        this.objectId = objectId;
        this.targetId = targetId;
    }
    public string message() {
        string retour = "{\"name\" : \"cast\",\"properties\" : {\"objectId\" : \"" + this.objectId + "\",\"targetId\" : \"" + this.targetId + "\"}}";
        return retour;
    }
}