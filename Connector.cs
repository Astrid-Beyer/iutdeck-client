using System;
using System.Text;
using System.Net.Sockets;

public interface IConnector {
    public IGameMessage send(IGameMessage message);
}