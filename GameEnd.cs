using System;
using System.Text;
using System.Net.Sockets;

public class GameEnd : IGameMessage {
    public string reason_code {get;set;}

    public GameEnd(string reason_code) { 
        this.reason_code = reason_code;
    }
    public string message() {
        string retour = "{\"name\" : \"game_end\",\"properties\" : {\"reason_code\" : \"" + this.reason_code + "\"}}";
        return retour;
    }
}