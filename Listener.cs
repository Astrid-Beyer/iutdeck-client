using System;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

public class Listener {
    private int    port;
    private string host;
    private TcpListener tcpListener;

    private bool isRunning;
    public Listener(int port, string host) {
        this.port           = port;
        this.host           = host;
        this.tcpListener    = null;
        this.isRunning      = false;
    }

    public void connect() {
        this.tcpListener = new TcpListener(IPAddress.Parse(this.host), this.port);
        this.isRunning   = true;
    }
    public void disconnect() {
        this.isRunning = false;
    }
    public void start() {
        if(tcpListener == null) this.connect();

        tcpListener.Start();

        byte[] bytes = new byte[1024]; //Creer un tableau d'octets pour stocker les messages entrants
        string data;

        while(isRunning){
            Console.Write("En attente d'une connexion... ");

            TcpClient connexionEntrante = tcpListener.AcceptTcpClient();
            Console.WriteLine("Connecté!");

            NetworkStream stream = connexionEntrante.GetStream();

            int i = stream.Read(bytes, 0, bytes.Length);

            while(i != 0) {
                // Récupère les données en string
                data = System.Text.Encoding.ASCII.GetString(bytes, 0, i);

                // Utilisation des données reçus
                IGameMessage message = GameClient.readMessage(data);
                
                IGameMessage retour = null;
                if(message is OpponentEvent){
                    try{
                        //Opponent event dans le jeu
                        retour = new Ok();
                    } catch (Exception e){
                        retour = new Error(500, e.Message);
                    }
                } else if(message is QuitRoom){
                    try{
                        //QuitRoom dans le jeu
                        GameClient.quitGame();
                        retour = new Ok();
                    } catch (Exception e){
                        retour = new Error(500, e.Message);
                    }
                } else if(message is GameStarting){
                    try{
                        //Game start dans le jeu
                        retour = new Ok();
                    } catch (Exception e){
                        retour = new Error(500, e.Message);
                    }
                } else if(message is Initialize){
                    try{
                        //Initialise dans le jeu
                        retour = new Ok();
                    } catch (Exception e){
                        retour = new Error(500, e.Message);
                    }
                } else if(message is AllSet){
                    try{
                        //AllSet dans le jeu
                        retour = new Ok();
                    } catch (Exception e){
                        retour = new Error(500, e.Message);
                    }
                } else if(message is GameStarted){
                    try{
                        //Game started dans le jeu
                        retour = new Ok();
                    } catch (Exception e){
                        retour = new Error(500, e.Message);
                    }
                } else if(message is YourTurn){
                    try{
                        //Your turn dans le jeu
                        retour = new Ok();
                    } catch (Exception e){
                        retour = new Error(500, e.Message);
                    }
                } else if(message is Cast){
                    try{
                        //Cast dans le jeu
                        retour = new Ok();
                    } catch (Exception e){
                        retour = new Error(500, e.Message);
                    }
                } else if(message is State){
                    try{
                        //State dans le jeu
                        retour = new Ok();
                    } catch (Exception e){
                        retour = new Error(500, e.Message);
                    }
                } else if(message is Surrender){
                    try{
                        //Surrender dans le jeu
                        retour = new Ok();
                    } catch (Exception e){
                        retour = new Error(500, e.Message);
                    }
                } else if(message is ObjectChange){
                    try{
                        //Object change dans le jeu
                        retour = new Ok();
                    } catch (Exception e){
                        retour = new Error(500, e.Message);
                    }
                } else if(message is GameEnd){
                    try{
                        //GameEnd dans le jeu
                        retour = new Ok();
                    } catch (Exception e){
                        retour = new Error(500, e.Message);
                    }
                } else if(message is Objet){
                    try{
                        //Object dans le jeu
                        retour = new Ok();
                    } catch (Exception e){
                        retour = new Error(500, e.Message);
                    }
                } else if(message is Error){
                    throw message.message;
                }  

                //On renvoie la réponse.
                byte[] sendBuffer = System.Text.Encoding.ASCII.GetBytes(retour.message());
                stream.Write(sendBuffer, 0, sendBuffer.Length);

                i = stream.Read(bytes, 0, bytes.Length);
            }

            connexionEntrante.Close();
        }
        tcpListener.Stop();
    }
}
