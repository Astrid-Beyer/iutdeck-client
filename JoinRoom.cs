using System;
using System.Text;
using System.Net.Sockets;

public class JoinRoom : IGameMessage {
    public int roomId {get;set;}
    public string gameToken {get;set;}

    public JoinRoom(int roomId, string gameToken) { 
        this.roomId    = roomId;
        this.gameToken = gameToken;
    }
    public string message() {
        string retour = "{\"name\" : \"joinRoom\",\"properties\" : {\"roomId\" : \"" + this.roomId + "\",\"gameToken\" : \"" + this.gameToken + "\"}}";
        return retour;
    }
}