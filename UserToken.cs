using System;
using System.Text;
using System.Net.Sockets;

public class UserToken : IGameMessage {
    public string token {get;set;}

    public UserToken(string token) { 
        this.token    = token;
    }
    public string message() {
        string retour = "{\"name\" : \"user_token\",\"properties\" : {\"token\" : \"" + this.token + "\"}}";
        return retour;
    }
}