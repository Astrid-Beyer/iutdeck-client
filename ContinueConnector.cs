using System;
using System.Text;
using System.Net.Sockets;

public class ContinueConnector : IConnector {
    private int    port;
    private string host;
    private TcpClient tcpClient;
    public ContinueConnector(int port, string host, TcpClient tcpClient) {
        this.port      = port;
        this.host      = host;
        this.tcpClient = null;
    }

    public void connect() {
        this.tcpClient = new TcpClient(this.host, this.port);
    }
    public void disconnect() {
        this.tcpClient.Close();
    }

    public IGameMessage send(IGameMessage message) {

        if(this.tcpClient == null) this.connect();

        NetworkStream ns = null;

        try { // tentative de connexion au flux
            ns = tcpClient.GetStream();
        } catch {
            Console.WriteLine("Impossible de se connecter au réseau, sortie du flux.");
                Environment.Exit(1);
        }

        Console.WriteLine("Connexion au flux réussie.");

        byte[] receiveBuffer = new byte[1024];
        byte[] sendBuffer;
        int bytesRead = 0;

        sendBuffer      = Encoding.ASCII.GetBytes(message.message());            // Transforme le message en suite d'octets
        ns.Write(sendBuffer, 0, sendBuffer.Length);                              // On écrit la suite d'octets dans le flux
        bytesRead       = ns.Read(receiveBuffer, 0, receiveBuffer.Length);       // On récupère la réponse du flux sous forme d'octets
        string response = Encoding.ASCII.GetString(receiveBuffer, 0, bytesRead); // Décode les données

        ns.Flush();
        ns.Close();
        ns.Dispose();

        return GameClient.readMessage(response);
    }
}