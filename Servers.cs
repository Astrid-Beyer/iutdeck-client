using System;
using System.Text;
using System.Net.Sockets;

public class Servers : IGameMessage {
    public string list {get;set;}

    public Servers(string list) { 
        this.list    = list;
    }
    public string message() {
        string retour = "{\"name\" : \"servers\",\"properties\" : {\"list\" : \"" + this.list + "\"}}";
        return retour;
    }
}