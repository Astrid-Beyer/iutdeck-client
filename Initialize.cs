using System;
using System.Text;
using System.Net.Sockets;

public class Initialize : IGameMessage {
    public string game_objects {get;set;}

    public Initialize(string game_objects) { 
        this.game_objects = game_objects;
    }
    public string message() {
        string retour = "{\"name\" : \"initialize\",\"properties\" : {\"game_objects\" : \"" + this.game_objects + "\"}}";
        return retour;
    }
}