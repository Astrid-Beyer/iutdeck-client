using System;
using System.Text;
using System.Net.Sockets;

public class ServerInfo : IGameMessage {
    public string name {get;set;}
    public string online {get;set;}
    public int rooms {get;set;}

    public ServerInfo(string name, string online, int rooms) { 
        this.name   = name;
        this.online = online;
        this.rooms  = rooms;
    }
    public string message() {
        string retour = "{\"name\" : \"server_info\",\"properties\" : {\"name\" : \"" + this.name + "\",\"online\" : \"" + this.online + "\",\"rooms\" : \"" + this.rooms +"\"}}";
        return retour;
    }
}