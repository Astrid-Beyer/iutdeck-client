using System;
using System.Text;
using System.Net.Sockets;

public class ServersList : IGameMessage {
    public string userToken {get;set;}

    public ServersList(string userToken) { 
        this.userToken = userToken;
    }
    public string message() {
        string retour = "{\"name\" : \"serversList\",\"properties\" : {\"userToken\" : \"" + this.userToken + "\"}}";
        return retour;
    }
}