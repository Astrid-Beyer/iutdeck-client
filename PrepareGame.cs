using System;
using System.Text;
using System.Net.Sockets;

public class PrepareGame : IGameMessage {
    public string userToken {get;set;}

    public PrepareGame(string userToken) { 
        this.userToken = userToken;
    }
    public string message() {
        string retour = "{\"name\" : \"prepareGame\",\"properties\" : {\"userToken\" : \"" + this.userToken + "\"}}";
        return retour;
    }
}