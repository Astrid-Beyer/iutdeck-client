using System;
using System.Text;
using System.Net.Sockets;

public class OpponentEvent : IGameMessage {
    public string evenement {get;set;}

    public OpponentEvent(string evenement) { 
        this.evenement = evenement;
    }
    public string message() {
        string retour = "{\"name\" : \"opponent_event\",\"properties\" : {\"event\" : \"" + this.evenement + "\"}}";
        return retour;
    }
}