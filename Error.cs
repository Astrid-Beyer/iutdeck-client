using System;
using System.Text;
using System.Net.Sockets;

public class Error : IGameMessage {
    public int code {get;set;}
    public string msg {get;set;}

    public Error(int code, string msg) { 
        this.code    = code;
        this.msg = msg;
    }
    public string message() {
        string retour = "{\"name\" : \"error\",\"properties\" : {\"code\" : \"" + this.code + "\",\"message\" : \"" + this.msg + "\"}}";
        return retour;
    }
}