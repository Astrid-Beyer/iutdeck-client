using System;
using System.Text;
using System.Net.Sockets;

public class Objet : IGameMessage {
    public string object_id {get;set;}
    public string type {get;set;}
    public string parent {get;set;}
    public string properties {get;set;}

    public Objet(string object_id, string type, string parent, string properties) { 
        this.object_id  = object_id;
        this.type       = type;
        this.parent     = parent;
        this.properties = properties;
    }
    public string message() {
        string retour = "{\"name\" : \"object\",\"properties\" : {\"object_id\" : \"" + this.object_id + "\",\"type\" : \"" + this.type + "\",\"parent\" : \"" + this.parent + "\",\"properties\" : \"" + this.properties + "\"}}";
        return retour;
    }
}