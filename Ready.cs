using System;
using System.Text;
using System.Net.Sockets;

public class Ready : IGameMessage {
    public bool ready {get;set;}

    public Ready(bool ready) { 
        this.ready = ready;
    }
    public string message() {
        string retour = "{\"name\" : \"ready\",\"properties\" : {\"ready\" : \"" + this.ready + "\"}}";
        return retour;
    }
}