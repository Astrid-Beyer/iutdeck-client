using System;
using System.Text;
using System.Net.Sockets;

public class State : IGameMessage {
    public int cardId {get;set;}
    public bool state {get;set;}

    public State(int cardId, bool state) { 
        this.cardId = cardId;
        this.state = state;
    }
    public string message() {
        string retour = "{\"name\" : \"state\",\"properties\" : {\"cardId\" : \"" + this.cardId + "\",\"state\" : \"" + this.state + "\"}}";
        return retour;
    }
}