// Classe principale -> va gérer les messages
using System;
using System.Collections.Generic;

public static class GameClient {

    private static int portAuth    = 49;
    private static string hostAuth = "127.0.0.1";
    private static OneShotConnector authConnector = new OneShotConnector(portAuth, hostGame);
    private static string userToken = null;
    private static int portGame     = null;
    private static string hostGame  = null;
    private static ContinueConnector gameConnector = null;
    private static Listener gameListener = null;
    private static string gameToken = null;

    public void connectToAuth(string username, string password){
        Auth authMess = new Auth(username, password);
        IGameMessage ret = authConnector.send(authMess);
        if(ret is UserToken){
            this.userToken = ret.token;
        } else {
            throw new Exception(ret.msg);
        }
    }

    public string askServerList(){
        if(userToken != null){
            ServersList slMess = new ServersList(userToken);
            IGameMessage ret = authConnector.send(slMess);
            if(ret is Servers){
            return ret.list;
            } else {
                throw new Exception(ret.msg);
            }
        }
        return null;
    }

    public void selectGameServer(int port, string hostname){
        this.portGame = port;
        this.hostGame = hostname;
        gameConnector = new ContinueConnector(portGame,hostGame);
        gameConnector.connect();
        gameListener  = new Listener(portGame,hostGame);
        gameListener.connect();
    }

    public void connectToServer(){
        if(userToken != null){
            PrepareGame pgMess = new Auth(userToken);
            IGameMessage ret = authConnector.send(pgMess);
            if(ret is GameToken){
                this.gameToken = ret.token;
            } else {
                throw new Exception(ret.msg);
            }
        }
    }

    public string getServerInfo(){
        if (gameConnector != null){
            Info infoMess = new Info();
            gameConnector.send(infoMess);
            if(ret is ServerInfo){
            return ret;
            } else {
                throw new Exception(ret.msg);
            }
        }
        return null;
    }

    public void joinRoom(int roomId){
        if (gameConnector != null){
            JoinRoom jrMess = new JoinRoom(roomId, gameToken);
            gameConnector.send(jrMess);
            if(ret is Ok){
            //On a bien join
            } else {
                throw new Exception(ret.msg);
            }
        }
    }

    public void joinRoomOffline(int roomId, string pseudo){
        if (gameConnector != null){
            JoinRoomOffline jroMess = new JoinRoomOffline(roomId, pseudo);
            gameConnector.send(jroMess);
            if(ret is Ok){
            //On a bien join
            } else {
                throw new Exception(ret.msg);
            }
        }
    }

    public void quitGame(){
        if (gameConnector != null){
            QuitRoom quitMess = new QuitRoom();
            gameConnector.send(quitMess);
            if(ret is Ok){
                this.portGame = null;
                this.hostGame = null;
                gameConnector.disconnect();
                gameConnector = null;
                gameListener.disconnect();
                gameListener = null;
            } else {
                throw new Exception(ret.msg);
            }
        }
    }

    public void notifyReady(bool isReady){
        if (gameConnector != null){
            Ready readyMess = new Ready(isReady);
            gameConnector.send(readyMess);
            if(ret is Ok){
                //
            } else {
                throw new Exception(ret.msg);
            }
        }
    }

    public void notifyAllSet(){
        if (gameConnector != null){
            AllSet asMess = new AllSet();
            gameConnector.send(asMess);
            if(ret is Ok){
                //
            } else {
                throw new Exception(ret.msg);
            }
        }
    }

    public void notifyCast(string object_id, string target_id){
        if (gameConnector != null){
            Cast castMess = new Cast(object_id,target_id);
            gameConnector.send(castMess);
            if(ret is Ok){
                //
            } else {
                throw new Exception(ret.msg);
            }
        }
    }

    public void notifyState(string object_id, string state){
        if (gameConnector != null){
            State stateMess = new State(object_id,state);
            gameConnector.send(stateMess);
            if(ret is Ok){
                //
            } else {
                throw new Exception(ret.msg);
            }
        }
    }

    public void notifySurrend(){
        if (gameConnector != null){
            Surrender ffMess = new Surrender();
            gameConnector.send(ffMess);
            if(ret is Ok){
                //
            } else {
                throw new Exception(ret.msg);
            }
        }
    }

    private static string readKey(string message, string keyName) {
        int index = message.IndexOf("\"" + keyName + "\" : \"");
        if( index == -1) throw new Exception("Message invalide!");
        index += 6 + keyName.Length;
        message = message.Substring(index);
        index = message.IndexOf("\"");
        string keyValue  = message.Substring(0, index);
        return keyValue;
    }
    private static Dictionary<string,string> getProperties(string message) {
        int index = message.IndexOf("\"properties\" : {");
        if( index == -1) throw new Exception("Message invalide!");
        index += 16;
        message = message.Substring(index);
        index = message.IndexOf("}");
        string properties = message.Substring(0, index);
        string[] propertiesArray = properties.Split(',');
        Dictionary<string,string> propertiesDict = new Dictionary<string, string>();

        foreach (string property in propertiesArray) {
            index = property.IndexOf("\"");
            string key = property.Substring(index+1);
            index = key.IndexOf("\"");
            key = key.Substring(0,index);
            string value = GameClient.readKey(property,key);
            propertiesDict.Add(key,value);
        }
        return propertiesDict;
    }
    public static IGameMessage readMessage(string message) {
        
        IGameMessage returnMessage = null;
        string messageType  = GameClient.readKey(message, "name");
        Dictionary<string,string> properties = GameClient.getProperties(message);

        switch (messageType) {
            case "ok":
                returnMessage = new Ok();
                break;
            case "error":
                //On converti
                returnMessage = new Error(
                    Int32.Parse(properties["code"]),
                    properties["message"]
                ); //On crée un GameMessage avec les params
                break;
            case "user_token":
                returnMessage = new UserToken(
                    properties["token"]
                );
                break;
            case "servers":
                returnMessage = new Servers(
                    properties["list"]
                );
                break;
            case "game_token":
                returnMessage = new GameToken(
                    properties["token"]
                );
                break;
            case "server_info":
                returnMessage = new ServerInfo(
                     properties["name"],
                     properties["online"],
                     Int32.Parse(properties["rooms"])
                );
                break;
            case "opponent_event":
                returnMessage = new OpponentEvent(
                    properties["event"]
                );
                break;
            case "quit_room":
                returnMessage = new QuitRoom();
                break;
            case "ready":
                bool ready;
                Boolean.TryParse(properties["ready"], out ready);
                returnMessage = new Ready(
                    ready
                );
                break;
            case "game_starting":
                returnMessage = new GameStarting();
                break;
            case "initialize":
                returnMessage = new Initialize(
                    properties["game_objects"]
                );
                break;
            case "your_turn":
                returnMessage = new YourTurn();
                break;
            case "cast":
                returnMessage = new Cast(
                    Int32.Parse(properties["object_id"]),
                    Int32.Parse(properties["target_id"])
                );
                break;
            case "state":
                bool state;
                Boolean.TryParse(properties["state"], out state);
                returnMessage = new State(
                    Int32.Parse(properties["card_id"]),
                    state
                );
                break;
            case "surrender":
                returnMessage = new Surrender();
                break;
             case "object_change":
                returnMessage = new ObjectChange(
                    properties["object_id"],
                    properties["properties"]
                );
                break;
            case "game_end":
                returnMessage = new GameEnd(
                    properties["reason_code"]
                );
                break;
            case "object":
                returnMessage = new Objet(
                    properties["object_id"],
                    properties["type"],
                    properties["parent"],
                    properties["properties"]
                );
                break;
            
            default:
                break;
        }

        return returnMessage;
    }
}