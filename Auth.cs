using System;
using System.Text;
using System.Net.Sockets;

public class Auth : IGameMessage {
    public string username {get;set;}
    public string password {get;set;}


    public Auth(string username, string password) { 
        this.username = username;
        this.password = password;
    }
    public string message() {
        string retour = "{\"name\" : \"auth\",\"properties\" : {\"username\" : \"" + this.username + "\",\"password\" : \"" + this.password + "\"}}";
        return retour;
    }
}