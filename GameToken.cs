using System;
using System.Text;
using System.Net.Sockets;

public class GameToken : IGameMessage {
    public string token {get;set;}

    public GameToken(string token) { 
        this.token    = token;
    }
    public string message() {
        string retour = "{\"name\" : \"game_token\",\"properties\" : {\"token\" : \"" + this.token + "\"}}";
        return retour;
    }
}