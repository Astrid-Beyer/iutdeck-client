using System;
using System.Text;
using System.Net.Sockets;

//GameMessage : interface template pour tous les msg de chaque évenement

public interface IGameMessage { 
    public string message();
}